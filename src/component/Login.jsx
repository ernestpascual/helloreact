import React from 'react';

const Login = (props) => 
    (
    <div className="App">
    <h1> {props.title} </h1>
    <input type="text" placeholder="username" onChange={props.change}/>
    <button onClick={props.click}> Click </button>

    </div>
  );

export default Login;

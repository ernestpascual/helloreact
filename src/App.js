import React, {  Component } from 'react';
import './App.css';
import Register from './component/Register'

// Register
// 1 component non stateful, stateful
// Username, password
// click register lalabas yung username chaks success

export default class App extends Component {
  state ={
    user: null,
    pw: null,
  
  }

  hello = event => {
    alert(this.state.greeting)
  }
  
  changer = event => {
    this.setState({
      greeting: event.target.value,

    })
  }

  /*
  06-17-2019

  Review
  - Set state
  - onChange event, onClick event

  React Router Demo
  - use a navbar
  - link to different routing
  - different views

  API calling 
  - fetch
  - setState
  - single entry calling, level up if kaya with loops
  
  http://jsonplaceholder.typicode.com/users

  */
    render() {
      return (
        <div>
          <Register title= {this.state.tite} change={this.changer}  click={this.hello} />
          </div>
        );
      }
    }

